import java.util.ArrayList;
import java.util.List;

public class Aquarium {
    private int hauteur;
    private int largeur;
    private List<PetitPoisson> petitsPoissons;
    //private List<GrosPoisson> grosPoissons;
    //private List<Algue> lesAlgues;
    //private List<Bulle> lesBulles;
    //private Dessin dessin;
    public Aquarium() {
        this.hauteur = 50;
        this.largeur = 80;
        this.petitsPoissons = new ArrayList<>();
        this.petitsPoissons.add(new PetitPoisson(70, 10, 1));
    }
    public int getHauteur() {
        return this.hauteur;
    }
    public int getLargeur() {
        return this.largeur;
    }
    public Dessin getDessin() {
        Dessin dessin = new Dessin();
        // pour les petits poissons
        for (PetitPoisson p : this.petitsPoissons) {
            dessin.union(p.getDessin());
        }
        return dessin;
    }
    public void evolue(){
        // pour les petits poissons
        for (PetitPoisson p : this.petitsPoissons) {
            if (p.getPosX() >= this.largeur-3 || p.getPosX() <= 0) {
                p.changeDirection();
            }
            p.nage();
        }
    }
}