public class GrosPoisson {
    private int posX;
    private int posY;
    private int vitesseX;
    private int direction;
    public GrosPoisson(int posX, int posY, int vitesseX) {
        this.posX = posX;
        this.posY = posY;
        this.vitesseX = vitesseX;
        this.direction = -1;
    }
    public void nage(){
        this.posX += this.vitesseX * this.direction;
    }
    public Dessin getDessin() {
        Dessin grosPoisson = new Dessin();
        String couleur = "0x0000F0";
        grosPoisson.ajouteChaine(this.posX, this.posY, "; ,//; , ,;/", couleur);
        grosPoisson.ajouteChaine(this.posX, this.posY-1, "o :::::::;;///", couleur);
        grosPoisson.ajouteChaine(this.posX, this.posY-2, ">::::::::;;\\\", couleur);
    } 
}
