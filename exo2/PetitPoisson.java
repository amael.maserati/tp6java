public class PetitPoisson {
    private int posX;
    private int posY;
    private int vitesseX;
    private int direction;
    public PetitPoisson(int posX, int posY, int vitesseX) {
        this.posX = posX;
        this.posY = posY;
        this.vitesseX = vitesseX;
        this.direction = 1;
    }
    public void nage(){
        this.posX += this.vitesseX * this.direction;
    }
    public void changeDirection() {
        this.direction = -this.direction;
    }
    public int getPosX() {
        return this.posX;
    }
    public Dessin getDessin() {
        Dessin petitPoisson = new Dessin();
        String couleur = "0x0000F0";
        if (this.direction == 1) {
            petitPoisson.ajouteChaine(this.posX, this.posY, "><>", couleur);
        }
        else {
            petitPoisson.ajouteChaine(this.posX, this.posY, "<><", couleur);
        }
        return petitPoisson;
    } 
}
